/**
 * Created by Catalin Glavan on 18-Mar-17.
 */

// import React, { Component, PropType } from 'react';
// import ReactDOM, { render } from 'react-dom';
// import { Router, Route , IndexLink, IndexRoute, Link, browserHistory } from 'react-router';


// import React, { Component } from 'react' ;
// export default class App extends Component { do something }         ---- ofera posibitiliatea de a nu mai scrie React.Component


// const App = function (props) { do something };               -----  scriere in ES5
// const App = (props) => { do something } ;                    -----  scriere in ES6
// clasele necesita this.props
// const nu necesita this.props / this.state


// <input onFocus = { (event) => { this.onFocusFuction(event); } /> -----   apelare functie cu prop

// const VideoList = ({videos}) => {                                  ----  daca este apelata cu acceas valoare in layout.js | ex. videos={this.state.videos}


// props are doua referinte ( key si refs ) !!!!

// ReactDOM.render(<app/>, document.getElementById("app");      need : <div id="app" />
// ReactDOM.render(<app/>, document.querySelector("app")        need : <div class="app" />


// < Router history = {browserHistory} routes ={<routes>}
// < Route path ="/" component = {<app>} />
// <nav> < IndexLink to="/page" activeClassName="active" /> </nav>
// <nav> < Link to="/page" activeClassName="active" /> </nav>


// const URL = "http://youtube/com/${videoID}" same like ----- const URL = "http://youtube/com/" + videoID;


// --------EXAMPLE CONST VARIABLES AND this.state OBJECTS-------------------------

/*
 export default class VideoItems extends Component {

 constructor(props) {
 super(props);

 this.state = {
 images: this.props.images,
 videoDesc: this.props.videoDesc,
 videoTime: this.props.videoTime,
 videoTitle: this.props.videoTitle
 };
 }

 render() {

 const getImage = this.state.images;
 const getVideoDesc = this.state.videoDesc;
 const getVideoTime = this.state.videoTime;
 const getvideoTitle = this.state.videoTitle;

 return ();
 }
 }
 */

// ----------------------------------------------------------------------------------

// ----- LODASH
// const videoSearch = _.debounce((event)  => { this.function(event)}, 300);        -- adauga un delay de 300 la executia functiei
