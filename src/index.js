import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import layout from './layout';
import home from './pages/home';
import playlist from './pages/playlist';
import history from './pages/history';
import about from './pages/about';
import {IndexRoute, Route, Router, browserHistory} from 'react-router';


const view = () => {
  return (
    <Route path="/" component={layout}>
      <IndexRoute component={home}/>
      <Route path="/playlist" component={playlist}/>
      <Route path="/history" component={history}/>
      <Route path="/about" component={about}/>
    </Route>
  );
};


render(
  <Router history={browserHistory} routes={view()}/>,
  document.getElementById("app")
);
