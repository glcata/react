import React, {Component} from 'react';
import MenuBar from '../components/menu_bar';
import VideoDataItems from '../components/video_components/video_data_items';


export default class playlist extends Component {

  constructor(props) {
    super(props);

    this.state = {
      title: "My Playlist",
      glyph: "glyphicon glyphicon-arrow-down",
      link: "/playlist",
      removeVideo: false
    };
  }

  render() {

    let dataVideos = require('../data/videosData.json');
    let dataObject = Object.values(dataVideos);             // ECMAScript 2017
    //console.log(JSON.stringify(dataObject));


    const message = () => {
      if (dataObject.length === 0) {
        return (<div><h3>Your playlist is empty.</h3></div>);
      }
    };

    const show_videos = dataObject.reverse().map((val) => {
      let formatVideoID = "videoID[" + val.id.videoId + "]";
      //console.log(formatVideoID);                             // Video ID Key
      return (<VideoDataItems key={val.etag} video={val} objKey={formatVideoID} selectRemoveVideo={() => {
        this.setState({removeVideo: true});
      }}/>);
    });

    return (
      <div>
        <MenuBar propTitle={this.state.title} propGlyph={this.state.glyph} />
        {message()}
        {show_videos}
      </div>
    );
  }
}
