import React, {Component} from 'react';
import MenuBar from '../components/menu_bar';
import VideoDataHistory from '../components/video_components/video_data_history';


let buttonOrder = false;
let clearHistory = false;

export default class history extends Component {

  constructor(props) {
    super(props);

    this.state = {
      title: "History",
      glyph: "glyphicon glyphicon-arrow-down",
      link: "/history",
      buttonHistory: "Data History",
      buttonIconHistory: "glyphicon glyphicon-chevron-up",
      clearState: null
    };
  }

  countObjects(obj) {
    let count = 0;
    for (let prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        ++count;
      }
    }
    return count;
  }

  orderVideos() {
    if (buttonOrder) {
      buttonOrder = false;
      this.setState({
        buttonHistory: "Data History(newest)",
        buttonIconHistory: "glyphicon glyphicon-chevron-up"
      });
    } else {
      buttonOrder = true;
      this.setState({
        buttonHistory: "Data History(oldest)",
        buttonIconHistory: "glyphicon glyphicon-chevron-down"
      });
    }
  }


  render() {

    // JSON Videos History
    let dataVideosHistory;
    let dataObjectHistory;
    if (!clearHistory) {
      dataVideosHistory = require('../data/videosHistory.json');
      dataObjectHistory = Object.values(dataVideosHistory);
    } else {
      dataObjectHistory = [];
      clearHistory = false;                                                           // CLEAR HISTORY
    }
    console.log(dataVideosHistory);                                                   // SHOW OBJECTS
    console.log("Number of objects: " + this.countObjects(dataVideosHistory));

    const message = () => {
      if (dataObjectHistory.length === 0) {
        return (<div><h3>Your history is empty.</h3></div>);
      }
    };

    function show_videos() {
      if (buttonOrder) {
        return dataObjectHistory.map((val) => {
          return (<VideoDataHistory key={val.etag} video={val}/>);
        });
      }
      else {
        return dataObjectHistory.reverse().map((val) => {
          return (<VideoDataHistory key={val.etag} video={val}/>);
        });
      }
    }

    return (
      <div className="text-left">
        <MenuBar propTitle={this.state.title} propGlyph={this.state.glyph} propLink={this.state.link}/>
        <button type="button" className="btn btn-default" onClick={() => {
          clearHistory = true;
          this.setState({clearState: true});
        }}>Clear History
        </button>
        <button type="button" className="btn btn-default" onClick={() => {
          this.orderVideos();
        }}>{this.state.buttonHistory}&nbsp;
          <span className={this.state.buttonIconHistory}/>
        </button>
        <div className="text-center">{message()}</div>
        <div className="row clearfix noGap">
          {show_videos()}
        </div>
      </div>
    );
  }
}
