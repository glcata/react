import React, {Component} from 'react';
import MenuBar from '../components/menu_bar';
import '../style/about_style.css';


export default class about extends Component {

  constructor(props) {
    super(props);

    this.state = {
      title: "About App",
      glyph: "glyphicon glyphicon-education",
      link: "/about"
    };
  }

  render() {

    let webpack = require('../style/images/webpack.png');
    let react = require('../style/images/react.png');
    let redux = require('../style/images/react-redux.png');
    let react_router = require('../style/images/react-router.png');
    let html_css_bootstrap = require('../style/images/html5+css3+bootstrap+es6+ps.png');

    return (
      <div className="youtube">
        <MenuBar propTitle={this.state.title} propGlyph={this.state.glyph} propLink={this.state.link}/>
        <div className="row">
          <div className="col-lg-3">
            <img className="webpack" src={webpack}/>
          </div>
          <div className="col-lg-3">
            <img className="react" src={react}/>
          </div>
          <div className="col-lg-3">
            <img className="redux" src={redux}/>
          </div>
          <div className="col-lg-3">
            <img className="react_router" src={react_router}/>
          </div>
        </div>
        <div className="row">
          <img className="html_css_ecma" src={html_css_bootstrap}/>
        </div>
      </div>
    );
  }
}
