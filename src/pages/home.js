import React, {Component} from 'react';
import YTSearch from 'youtube-api-search';
import SearchBar from '../components/search_bar';
import VideoList from '../components/video_components/video_list';
import VideoDetail from '../components/video_components/video_detail';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../style/style.css';

const API_KEY = "AIzaSyALI0hoV9mH745BdRlOLD_zWCRaVDSHgGg";
let default_search = "john sonmez";
let string_search;

export default class home extends Component {

  constructor() {
    super();

    this.state = {
      videos: [],
      selected: null,
      search: default_search
    };
    this.videoSearch(default_search);
  }

  videoSearch(value) {
    string_search = value;
    //console.log(default_search);

    YTSearch({key: API_KEY, term: string_search},
      (data) => {
        this.setState({
          videos: data,
          selected: data[this.randomNumber(data.length)],
          search: string_search
        });
        //console.log(data);
      });
  }

  randomNumber(interval) {
    return (Math.floor(Math.random() * interval));
  }

  render() {

    return (
      <div>
        <SearchBar
          onSearch={(event) => {
            this.videoSearch(event);
          }}
          ID={this.state.selected}
          defaultSearch={default_search}
        />
        <div className="row">
          <div className="col">
            <VideoDetail elem={this.state.selected}/>
          </div>
          <div className="col gap">
            <VideoList
              onVideoSelected={(selected) => {
                this.setState({selected});
              }}
              elem={this.state.videos}/>
          </div>
        </div>
      </div>
    );
  }
}

