import React, {PropTypes, Component} from 'react';
import {InputGroup, FormGroup} from 'react-bootstrap';
import {Link} from 'react-router';
import DropDown from './dropdown';

export default class menu_bar extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    let titlePage = this.props.propTitle;
    let glyph = "menu " + this.props.propGlyph;

    return (
      <div className="bar">
        <FormGroup>
          <InputGroup>
            <InputGroup.Addon className="btn btn-custom text-left">
              <Link to="/" activeClassName="active">
                <span className="glyphicon glyphicon-home"/>
              </Link>
            </InputGroup.Addon>
            <InputGroup.Addon className="btn-custom2">
                <span className="play_list"> {titlePage} </span>
                <span className={glyph}/>
            </InputGroup.Addon>
            <div className="form-control btn-danger text-left"/>
            <InputGroup.Addon className="btn-danger noGap">
              <DropDown/>
            </InputGroup.Addon>
          </InputGroup>
        </FormGroup>
      </div>
    );
  }
}

menu_bar.propTypes = {
  propTitle: PropTypes.string,
  propGlyph: PropTypes.string
};
