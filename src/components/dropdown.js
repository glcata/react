import React, {Component} from 'react';
import {DropdownButton, MenuItem} from 'react-bootstrap';
import {Link} from 'react-router';

export default class dropdown extends Component {

  render() {

    let set = "";
    let pages = ["playlist", "about", "history"];
    const getDOM_URL = document.URL;

    let check = false;
    pages.map((val) => {
      if (getDOM_URL.includes(val)) {
        check = true;
      }
    });
    if (check) {
      return (
        <DropdownButton className="btn-danger noGap glyphicon glyphicon-align-justify" title={set} bsStyle="default"
                        dropdown pullRight noCaret id="dropdown-no-caret">
          <MenuItem eventKey="1">
            <Link to="/" activeClassName="active">
              <strong>Search Video</strong>
            </Link>
          </MenuItem>
          <MenuItem eventKey="2">
            <Link to="/playlist" activeClassName="active">
              <strong>My Playlist</strong>
            </Link>
          </MenuItem>
          <MenuItem eventKey="3">
            <Link to="/history" activeClassName="active">
              <strong>History</strong>
            </Link>
          </MenuItem>
          <MenuItem divider/>
          <MenuItem eventKey="4">
            <Link to="/about" activeClassName="active">
              <strong>About</strong>
            </Link>
          </MenuItem>
        </DropdownButton>
      );
    } else {
      return (
        <DropdownButton className="btn-danger noGap glyphicon glyphicon-align-justify" title={set} bsStyle="default"
                        dropdown pullRight noCaret id="dropdown-no-caret">
          <MenuItem eventKey="1">
            <Link to="/" activeClassName="active">
              <strong>Search Video</strong>
            </Link>
          </MenuItem>
          <MenuItem eventKey="2">
            <Link to="/history" activeClassName="active">
              <strong>History</strong>
            </Link>
          </MenuItem>
          <MenuItem divider/>
          <MenuItem eventKey="3">
            <Link to="/about" activeClassName="active">
              <strong>About</strong>
            </Link>
          </MenuItem>
        </DropdownButton>
      );
    }
  }
}
