import React, {Component, PropTypes} from 'react';
import {InputGroup, FormGroup, FormControl} from 'react-bootstrap';
import DropDown from './dropdown';
import {Link} from 'react-router';
import '../style/menu.css';

let check = null;

export default class search_bar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      place_holder: "Search..."
    };
  }

  onFormChange(event) {
    /*const DOM = document.createElement('div');
     DOM.innerHTML = event.target.value;
     document.getElementById('text').appendChild(DOM);
     console.log(event.target.value);*/
    this.props.onSearch(event.target.value);
    check = event.target.value;
  }

  onFormFocus() {
    this.setState({
      place_holder: " "
    });
    //console.clear();
  }

  onFormFocusOut() {
    this.setState({
      place_holder: "Search..."
    });
  }

  render() {

    let URL_ID;
    if (this.props.ID != null) {
      URL_ID = "https://www.youtube.com/watch?v=" + this.props.ID.id.videoId;
    }
    if (check == "") {
      this.props.onSearch(this.props.defaultSearch);
      check = null;
    }

    //console.log(this.props.ID);

    return (
      <div className="bar">
        <FormGroup>
          <InputGroup>
            <InputGroup.Addon className="btn btn-custom text-left">
              <a href={URL_ID}><span className="glyphicon glyphicon-search"/></a>
            </InputGroup.Addon>
            <FormControl id="txt" className="btn-warning text-left"
                         onChange={(event) => {
                           this.onFormChange(event);
                         }}
                         onFocus={() => {
                           this.onFormFocus();
                         }}
                         onBlur={() => {
                           this.onFormFocusOut();
                         }}
                         placeholder={this.state.place_holder}
                         type="text"
            />
            <InputGroup.Addon className="btn btn-custom2">
              <Link to="/playlist" activeClassName="active">
                <span className="play_list"> My Playlist </span>
                <span className="menu glyphicon glyphicon-music"/>
              </Link>
            </InputGroup.Addon>
            <InputGroup.Addon className="btn-danger noGap">
              <DropDown/>
            </InputGroup.Addon>
          </InputGroup>
        </FormGroup>
      </div>
    );
  }
}

search_bar.propTypes = {
  onSearch: PropTypes.func,
  ID: PropTypes.object,
  'ID.id': PropTypes.object,
  'ID.id.videoId': PropTypes.object,
  defaultSearch: PropTypes.string
};
