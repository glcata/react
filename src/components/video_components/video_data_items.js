import React, {Component, PropTypes} from 'react';

export default class VideoDataItems extends Component {

  constructor(props) {
    super(props);
  }

  removeVideo(objKey) {
    let dataVideos = require('../../data/videosData.json');
    delete dataVideos[objKey];
    this.props.selectRemoveVideo();
    console.log(dataVideos);                                 // Current videosData.json
  }

  render() {
    const getImage = this.props.video.snippet.thumbnails.default.url;
    const getVideoDesc = this.props.video.snippet.description;
    const getVideoTime = this.props.video.snippet.publishedAt;
    const getvideoTitle = this.props.video.snippet.title;
    const getObjKey = this.props.objKey;

    return (
      <li className="list-group-item">
        <div className="list-group-item hoverOff text-left">
          <small>VIDEO TITLE :&nbsp;</small>
          {getvideoTitle}
        </div>
        <div className="media-left">
          <img className="pull-left" src={getImage}/>
        </div>
        <div className="media-right text-left">
          Description :
          <p className="text-left descSize">{getVideoDesc}</p>
          <p className="text-left small">{getVideoTime}</p>
          <button type="button" className="btn btn-warning btn-sm" onClick={() => {
            this.removeVideo(getObjKey);
          }}>REMOVE VIDEO
          </button>
        </div>
      </li>
    );
  }
}

VideoDataItems.propTypes = {
  onVideoSelected: PropTypes.func,
  video: PropTypes.object,
  objKey: PropTypes.string,
  selectRemoveVideo: PropTypes.func,
  'video.snippet': PropTypes.object,
  'video.snippet.title': PropTypes.string,
  'video.snippet.thumbnails': PropTypes.object,
  'video.snippet.thumbnails.default': PropTypes.object,
  'video.snippet.thumbnails.default.url': PropTypes.object,
  'video.snippet.description': PropTypes.object,
  'video.snippet.publishedAt': PropTypes.object
};
