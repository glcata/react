import React, {PropTypes, Component} from 'react';
import {InputGroup} from 'react-bootstrap';
import ReactFBLike from 'react-fb-like';

let dataVideos = {};

export default class VideoDetail extends Component {

  constructor(props) {
    super(props);

    this.state = {
      videoAdded: false
    };
  }

  countObjects(obj) {
    let count = 0;
    for (let prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        ++count;
      }
    }
    return count;
  }

  addDataVideo(obj) {
    // JSON Videos
    dataVideos = require('../../data/videosData.json');
    let formatVideoID = "videoID[" + this.props.elem.id.videoId + "]";
    dataVideos[formatVideoID] = obj;
    this.setState({videoAdded: true});
    //console.log(dataVideos);                                                  // VIDEO DATA ADD PLAYLIST
    //console.log("Number of objects: " + this.countObjects(dataVideos));
    //console.log(this.props.elem);                                             // ONLY ADDED OBJECT
  }

  render() {

    if (this.props.elem == null) {
      return (<div className="col-md-5 text-left"><strong>Loading...</strong></div>);
    }

    const URL = "https://www.youtube.com/embed/" + this.props.elem.id.videoId;


    // JSON Videos History
    let dataVideosHistory = require('../../data/videosHistory.json');
    let formatVideoID = "videoID[" + this.props.elem.id.videoId + "]";
    dataVideosHistory[formatVideoID] = this.props.elem;
    //console.log(JSON.stringify(dataVideosHistory));                                               // VIDEO HISTORY
    //console.log("Number of objects: " + this.countObjects(dataVideosHistory));

    //console.log(JSON.stringify(dataVideos));                                    // VIDEO ADD PLAYLIST

    const checkButton = () => {
      if (formatVideoID in dataVideos) {
        return (
          <InputGroup className="media-left">
            <InputGroup.Addon className="btn-custom2">
              <span className="glyphicon glyphicon-check noButtonGap"/>
            </InputGroup.Addon>
            <InputGroup.Addon className="btn btn-warning btn-sm">
              <strong>Video added</strong>
            </InputGroup.Addon>
          </InputGroup>
        );
      } else return (
        <InputGroup className="media-left">
          <InputGroup.Addon className="btn-custom2">
            <span className="glyphicon glyphicon-new-window noButtonGap"/>
          </InputGroup.Addon>
          <InputGroup.Addon className="btn btn-danger btn-sm" onClick={() => {
            this.addDataVideo(this.props.elem);
          }}>
            <strong>Add to playlist</strong>
          </InputGroup.Addon>
        </InputGroup>
      );
    };

    return (
      <div className="video-detail col-md-7">
        <div className="embed-responsive embed-responsive-16by9">
          <iframe className="embed-responsive-item" src={URL}/>
        </div>
        <div className="details text-left">
          <br/>
          <div className="row">
            <div className="col col-lg-8">
              <h5>
                <strong>
                  <small> TITLE :&nbsp;</small>
                  {this.props.elem.snippet.title}
                </strong>
              </h5>
            </div>
            <div className="col col-lg-3">
              {checkButton()}
            </div>
          </div>
          <div className="row">
            <div className="col col-lg-8">
              <small> Description :</small>
              <br/>
              {this.props.elem.snippet.description}
              <br/><br/>
              <ReactFBLike language="en_US" appId="717589285046812" version="v2.8"/><br/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

VideoDetail.propTypes = {
  elem: PropTypes.object,
  'elem.id': PropTypes.object,
  'elem.id.videoId': PropTypes.object,
  'elem.snippet': PropTypes.object,
  'elem.snippet.title': PropTypes.object
};
