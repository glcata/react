import React, {Component, PropTypes} from 'react';
import VideoItems from './video_list_items';


export default class VideoList extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    const videos_items = this.props.elem.map((val) => {
      return (
        <VideoItems onVideoSelected={this.props.onVideoSelected}
                    key={val.etag}
                    video={val}
        />);
    });

    return (
      <ul className="col-md-5 list-group clearfix">
        {videos_items}
      </ul>
    );
  }
}

VideoList.propTypes = {
  elem: PropTypes.array,
  onVideoSelected: PropTypes.func
};


