import React, {Component, PropTypes} from 'react';


export default class VideoItems extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const onVideoSelected = this.props.onVideoSelected;
    const getImage = this.props.video.snippet.thumbnails.default.url;
    const getVideoDesc = () => {
      let maxLetter = 120;
      let checkString = this.props.video.snippet.description;
      return ((checkString.length <= maxLetter) ? checkString : checkString.substring(0, maxLetter) + "...");
    };
    const getVideoTime = this.props.video.snippet.publishedAt;
    const getvideoTitle = this.props.video.snippet.title;

    return (
      <li className="list-group-item" onClick={() => onVideoSelected(this.props.video)}>
        <div className="list-group-item hoverOff text-left">
          <small>VIDEO TITLE :&nbsp;</small>
          {getvideoTitle}
        </div>
        <div className="media-left">
          <img className="pull-left" src={getImage}/>
        </div>
        <div className="media-right text-left">
          Description :
          <p className="text-left small descSize">{getVideoDesc()}</p>
          <p className="text-left small">{getVideoTime}</p>
        </div>
      </li>
    );
  }
}

VideoItems.propTypes = {
  onVideoSelected: PropTypes.func,
  video: PropTypes.object,
  'video.snippet': PropTypes.object,
  'video.snippet.title': PropTypes.string,
  'video.snippet.thumbnails': PropTypes.object,
  'video.snippet.thumbnails.default': PropTypes.object,
  'video.snippet.thumbnails.default.url': PropTypes.object,
  'video.snippet.description': PropTypes.object,
  'video.snippet.publishedAt': PropTypes.object
};
