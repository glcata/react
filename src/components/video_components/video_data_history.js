import React, {Component, PropTypes} from 'react';

export default class VideoDataHistory extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    const URL_ID = "https://www.youtube.com/watch?v=" + this.props.video.id.videoId;
    const getImage = this.props.video.snippet.thumbnails.default.url;
    const getVideoDesc = () => {
      let maxLetter = 75;
      let checkString = this.props.video.snippet.description;
      return ((checkString.length <= maxLetter) ? checkString : checkString.substring(0, maxLetter) + "...");
    };
    const getvideoTitle = () => {
      let maxLetter = 26;
      let checkString = this.props.video.snippet.title;
      return ((checkString.length <= maxLetter) ? checkString : checkString.substring(0, maxLetter) + "...");
    };

    return (
      <div className="col-md-4 noGapHistory">
        <div className="col">
          <li className="list-group-item" onClick={() => {
            window.open(URL_ID, '_blank');
          }}>
            <div className="list-group-item hoverOff text-left noTitleResize">
              {getvideoTitle()}
            </div>
            <div className="media-left">
              <img className="pull-left" src={getImage}/>
            </div>
            <div className="media-right">
              Description :
              <p className="descSize">{getVideoDesc()}</p>
            </div>
          </li>
        </div>
      </div>
    );
  }
}

VideoDataHistory.propTypes = {
  video: PropTypes.object,
  'video.id.videoId': PropTypes.string,
  'video.snippet': PropTypes.object,
  'video.snippet.title': PropTypes.string,
  'video.snippet.thumbnails': PropTypes.object,
  'video.snippet.thumbnails.default': PropTypes.object,
  'video.snippet.thumbnails.default.url': PropTypes.object,
  'video.snippet.description': PropTypes.object
};
