import React, {Component} from 'react';
import {InputGroup, FormGroup} from 'react-bootstrap';
import Modal from 'react-awesome-modal';

export default class CopyRight extends Component {

  constructor(props) {
    super(props);

    this.state = {
      visible: false
    };
  }

  openModal() {
    this.setState({
      visible: true
    });
  }

  closeModal() {
    this.setState({
      visible: false
    });
  }

  render() {

    let date = new Date();
    let URL_image = require('../style/images/qr-metro-logo.jpg');
    const QRCode = require('qrcode-react');
    const getDOM_URL = document.URL;


    // OTHER WAY QR-CODE
    /*<div className="qrcode">
     <img className="qrcode" src={URL_image}/>
     </div>*/

    return (
      <FormGroup className="copyright">
        <InputGroup>
          <InputGroup.Addon className="btn-custom2 text-center youtube copyright">
            <small>
              Copyright © {date.getFullYear()} METRO SYSTEMS. All rights reserved.
            </small>
          </InputGroup.Addon>
          <InputGroup.Addon className="btn btn-custom text-center" onClick={() => this.openModal()}>
            <span className="glyphicon glyphicon glyphicon-qrcode"/>
          </InputGroup.Addon>
        </InputGroup>
        <section>
          <Modal
            visible={this.state.visible}
            width="290"
            height="290"
            effect="fadeInUp"
            onClickAway={() => this.closeModal()}
          >
            <br/>
            <div className="col-md-5"><QRCode
              value={getDOM_URL}
              size={256}
              logo={URL_image}
              bgColor={"#ffffff"}
              fgColor={"#000000"}
              level={"H"}
            />
            </div>
          </Modal>
        </section>
      </FormGroup>
    );
  }
}


