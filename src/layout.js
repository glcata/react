import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router';
import CopyRight from './components/copy_right';

export default class App extends Component {


  render() {

    let URL_image = require('./style/images/bg.jpg');

    return (
      <div className="container-fluid">
        <div className="layout">
          <div>
            <Link to="/" activeClassName="active">
              <img className="youtube" src={URL_image}/>
            </Link>
          </div>
          {this.props.children}
        </div>
        <CopyRight/>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired
};
